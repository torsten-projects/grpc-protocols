# gRPC Protocols
[![PyPI version](https://badge.fury.io/py/freem-protocols.svg)](https://badge.fury.io/py/freem-protocols)
[![npm version](https://badge.fury.io/js/freem-protocols.svg)](https://badge.fury.io/js/freem-protocols)

This repository contains proto3 files for all my projects using gRPC.
Each release is automatically built and pushed to the respective language's repository.
